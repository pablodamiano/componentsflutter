

import 'package:components_futer/src/pages/input_page.dart';
import 'package:components_futer/src/pages/list_view_page.dart';
import 'package:components_futer/src/pages/slider_page.dart';
import 'package:flutter/material.dart';
import 'package:components_futer/src/pages/alert_page.dart';
import 'package:components_futer/src/pages/avatar_page.dart';
import 'package:components_futer/src/pages/home_page.dart';
import 'package:components_futer/src/pages/card_page.dart';
import 'package:components_futer/src/pages/container_animated.dart';

Map<String,WidgetBuilder> getAplicationRoutes(){

return  <String,WidgetBuilder>{
        '/'       : ( BuildContext context)=> HomePage(),
        'alert'   : ( BuildContext context)=> AlertPage(),
        'avatar'  : ( BuildContext context)=> AvatarPage(),
        'card'    : ( BuildContext context)=> CardPage(),
        'animatedContainer'    : ( BuildContext context)=> ContainerAnimated(),
        'inputs'    : ( BuildContext context)=> InputPage(),
        'slider'    : ( BuildContext context)=> SliderPage(),
        'list'    : ( BuildContext context)=> ListPage(),
      };
}

