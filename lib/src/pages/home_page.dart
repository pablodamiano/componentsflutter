import 'package:flutter/material.dart';

import 'package:components_futer/src/providers/menu_provider.dart';
import 'package:components_futer/src/utils/icono_string_util.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('My app'),
      ),
      body: _lista(),
          );
        }

  Widget _lista() {
     return FutureBuilder(
        initialData:[],
        future:menuprovider.cargardata(),
        builder:(context,AsyncSnapshot<List<dynamic>> snapshot){
          return ListView(
            children: _listaItems(snapshot.data,context)
     
          );
         },
       );
    
  }

  List<Widget> _listaItems(List<dynamic> data,BuildContext context) {
    ////////////////////////////////////////////////////
    ///corregir error de foreach with 'if(data = null)=> return list void, o podemos
    ///habilitar initialdata: parad ecir que data no esta vacia///////
    final List<Widget> opciones = [];
    data.forEach((opt) {
      final Widget widgeTemp = ListTile(
        title: Text(opt['texto']),
        trailing: Icon(Icons.arrow_forward_ios, color: Colors.blueGrey),
        leading: getIcon(opt['icon']),
        onTap: (){
           Navigator.pushNamed(context, opt['ruta']);
        },
      );
      opciones..add(widgeTemp)
              ..add(Divider());    
    });
    return opciones;
  }
}
      
      
      