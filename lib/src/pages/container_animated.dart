import 'dart:math';

import 'package:flutter/material.dart';
class ContainerAnimated extends StatefulWidget {
  @override
  _ContainerAnimatedState createState() => _ContainerAnimatedState();
}

class _ContainerAnimatedState extends State<ContainerAnimated> {

  double _width  = 50.0;
  double _heigth = 50.0;
  Color  _color  = Colors.pink;

  BorderRadiusGeometry _borderRadius = BorderRadius.circular(8.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ContainerAnimated'),

      ),
      body: Center(
        child: AnimatedContainer(
          curve:Curves.fastLinearToSlowEaseIn,
          duration: Duration(
            seconds:1,
          ),
          width: _width,
          height:_heigth,
          decoration: BoxDecoration(
            borderRadius: _borderRadius,
            color: _color,
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed:_cambiarForma,
        child: Icon(Icons.play_arrow),
      ),
    );
  }

  _cambiarForma() {
    final ramdom = Random();
    setState(() {
            _width =ramdom.nextInt(300).toDouble();
            _heigth = ramdom.nextInt(300).toDouble();
            _color = Color.fromRGBO(
              ramdom.nextInt(225),
              ramdom.nextInt(225),
              ramdom.nextInt(225),
              1
            );
            _borderRadius = BorderRadius.circular(ramdom.nextInt(100).toDouble());
    });
  }
}