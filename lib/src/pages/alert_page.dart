
import 'package:flutter/material.dart';

class AlertPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar:AppBar(
          title: Text('Alert Page'),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add_location),
          onPressed:(){
            Navigator.pop(context);
          }
          
          
        ),
        body: Center(
          child: RaisedButton(
            child: Text('Mostrar Alerta'),
            color: Colors.blue,
            onPressed: (){mostrarAlert(context);},
            textColor: Colors.white,  
            shape: StadiumBorder(), 
          ),
        ),
    );
  }
  void mostrarAlert(BuildContext context){
    showDialog(
      context:context,
      builder: (context){return AlertDialog(
        shape:RoundedRectangleBorder(borderRadius:BorderRadius.circular(20.0)),
        //backgroundColor: Colors.blueGrey,
        title: Text('Alerta¡'),
        content:Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text('Después de haber estado desaparecido durante casi 20 años, Rick Sánchez llega de imprevisto a la puerta de la casa de su hija Beth y se va a vivir con ella y su familia utilizando el garaje como su laboratorio personal.'),
            FlutterLogo(size: 50.0,),
          ],
        ),
        actions: <Widget>[
          FlatButton(
            onPressed:(){Navigator.of(context).pop();},
            child: Text('Cancel')),
          FlatButton(
            onPressed: (){Navigator.of(context).pop();},
            child: Text('ok'))
        ],
      );},
      
    );
  }
}