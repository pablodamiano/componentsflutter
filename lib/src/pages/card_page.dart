import 'package:flutter/material.dart'; 

class CardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CarPage'),
        
      ),
      body: ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          _cardTipoUno(),
          SizedBox(height: 30.0,),
          _cardTipo2(),
          SizedBox(height: 30.0,),
           _cardTipoUno(),
          SizedBox(height: 30.0,),
          _cardTipo2(),
          SizedBox(height: 30.0,),
           _cardTipoUno(),
          SizedBox(height: 30.0,),
          _cardTipo2(),
          SizedBox(height: 30.0,),
           _cardTipoUno(),
          SizedBox(height: 30.0,),
          _cardTipo2(),
          SizedBox(height: 30.0,),
           _cardTipoUno(),
          SizedBox(height: 30.0,),
          _cardTipo2(),
          SizedBox(height: 30.0,),
        ],
      ),
    );
  }

  Widget _cardTipoUno() {
      return Card(
        elevation: 10.0,
        shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(20.0)) ,
        child: Column(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.account_balance,color: Colors.blueGrey,),
              title:Text('Chapo And Over'),
              subtitle: Text('Después de haber estado desaparecido durante casi 20 años, Rick Sánchez llega de imprevisto a la puerta de la casa de su hija Beth y se va a vivir con ella y su familia utilizando el garaje como su laboratorio personal.'),

            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
             children: <Widget>[
               FlatButton(
                 child: Text('Cancelar'),
                 onPressed: (){},
               ),
               FlatButton(
                 child: Text('OK'),
                 onPressed: (){},
               ),
             ],  
            ),
          ],
        ),
      );
  }

  Widget _cardTipo2() {
    final card = Container(
    // clipBehavior: Clip.antiAlias,
      child: Column(
        children: <Widget>[
          FadeInImage(
            placeholder: AssetImage('assets/original.gif'),
            image:NetworkImage('https://as.com/epik/imagenes/2019/07/17/portada/1563350726_690723_1563352479_noticia_normal.jpg'),
            fadeInDuration: Duration(milliseconds: 200),
            height: 250.0,
            fit: BoxFit.cover,
            ),
          //Image(image: NetworkImage('https://as.com/epik/imagenes/2019/07/17/portada/1563350726_690723_1563352479_noticia_normal.jpg') ),
          Container(
            padding: EdgeInsets.all(10.0),
            child: Text('Rick And Your Bath'),

          ),
        ],
      ),
    );
    return Container(
     
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30.0),
        color: Colors.white,
        boxShadow: <BoxShadow>[
          BoxShadow(
            color:Colors.black26,
            blurRadius: 10.0,
            spreadRadius: 2.0,
            offset: Offset(2.0,10.0),
          ),
        ]
      ),
       child: ClipRRect(
         borderRadius: BorderRadius.circular(30.0),

         child: card,
       ),
    );
  }
}