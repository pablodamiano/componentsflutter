import 'dart:async';

import 'package:flutter/material.dart';

class ListPage extends StatefulWidget {
  //ListPage({Key key}) : super(key: key);
  
  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  List<int> _listaNumeros = new List();
  int _ultimoItem = 0;
  ScrollController _scrollController = new ScrollController();

  bool _isLoading = false;

  @override
    void initState() {
      super.initState();
      _agregar10();
      _scrollController.addListener(() {
        if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
          //_agregar10();
          fetData();
        }
      });
    }
  @override
  void dispose() {
   
    super.dispose();
    _scrollController.dispose();
  }  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ListPage'),
      ),
      body:Stack(
        children: <Widget>[
          _crearLista(),
          _crearLoading(),
        ],
      )
       
    );
  }
  Widget _crearLista(){
    return RefreshIndicator(
        onRefresh: _obtenerPagina1 ,  
         child: ListView.builder(
          //padding: EdgeInsets.all(0.0),
          controller: _scrollController,
          itemCount:_listaNumeros.length,
          itemBuilder: (BuildContext context,int index){
            final imagen = _listaNumeros[index];
            return Container(
              color:Colors.white,
              width: 500.0,
              height:216.0,
              child: FadeInImage(
                placeholder: AssetImage('assets/original.gif'),
                image: NetworkImage('https://picsum.photos/500/300/?image=$imagen'),
                fadeInDuration: Duration(milliseconds: 200),
                //fit: BoxFit.cover,
              ),
            );
          }
        ),
    );
  }

  void _agregar10(){
    for (var i = 0; i < 10; i++) {
      _ultimoItem++;
      _listaNumeros.add(_ultimoItem);
    
    }
      setState(() {
        
      });
  }

  Future<Null> fetData() async{
    _isLoading = true;
    setState((){});

    final duration = new Duration(seconds: 2);
    return Timer(duration,respuestaHTTP);   

  }

  void respuestaHTTP(){
    _isLoading = false;
    _scrollController.animateTo(
      _scrollController.position.pixels +100,
      duration: Duration(seconds: 2), 
      curve: Curves.fastOutSlowIn);
    _agregar10();
  }

  Widget _crearLoading() {

    if (_isLoading) {
      return Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
               CircularProgressIndicator()
            ],
          ),
          SizedBox(height: 15.0,)
        ],
     
      );
      
     
      
    }else{

      return Container();
    }
  }

  Future<Null> _obtenerPagina1()async{

    final duration = Duration(seconds:2 );
    new Timer(
      duration, 
      (){
        _listaNumeros.clear();
        _ultimoItem++;
        _agregar10();
      });
    return Future.delayed(duration);
  }
  
}

