import 'package:flutter/material.dart';

class AvatarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar:AppBar(
          title: Text('Avatar Page'),
          actions: <Widget>[
            Container(
              padding: EdgeInsets.all(7.0),
              child: CircleAvatar(
                backgroundImage: NetworkImage('https://starbaseatlanta.com/wp-content/uploads/rmhprs1257-600x604.jpg'),
                radius: 22.0,
              ),
            ),
            Container(
              margin: EdgeInsets.only(right:10.0),
              child: CircleAvatar(
                child: Text('SL'),
                backgroundColor: Colors.brown,

              ),
            ),
          ],
        ),
        body:  Center(
          child: FadeInImage(
            placeholder: AssetImage('assets/original.gif'),
            image: NetworkImage('https://i.pinimg.com/originals/f1/2a/07/f12a070f4136ab8ea3fbbddb425b6e48.png'),
            fadeInDuration: Duration(milliseconds: 200),
            ),  
        ),
    );
  }
}