import 'package:flutter/material.dart';
class SliderPage extends StatefulWidget {
  @override
  _SliderPageState createState() => _SliderPageState();
}

class _SliderPageState extends State<SliderPage> {
  double _valorSlider = 100.0;
  bool   _bloquearcheackbox=false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
        title: Text('SliderPage'),
      ),
     body: Container(
       padding: EdgeInsets.only(top:50.0),
          child: Column(
            children: <Widget>[
              _createSlider(),
              _crearCheckBox2(),
              _crearSwitch(),
              _crearCheckBox(),
              Expanded(child: _crearImagen()),
            ],
          ),
     ),
    );
  }

  Widget _createSlider() {
      return Slider(
        activeColor: Colors.indigoAccent,
        label: 'Tamaño Imagen',
        value:_valorSlider,
        ///divisions: 20,
        min: 10.0,
        max: 400.0, 
        onChanged:(_bloquearcheackbox)? null:
        (valorr){
          setState(() {
            _valorSlider =valorr;
          });
        });
  }

  Widget _crearImagen() {
    return Image(
      image: NetworkImage('https://i.pinimg.com/originals/98/29/21/9829215db6f9210c0ae4e318e854cb1f.png'),
      width: _valorSlider,
      fit: BoxFit.contain,
    );
  }

  Widget _crearCheckBox() {
    return Checkbox(
      value: _bloquearcheackbox, 
      onChanged:(value){
       setState(() {
          _bloquearcheackbox =value;
       });
      }
    );
  }

  Widget _crearCheckBox2() {
    return CheckboxListTile(
      title: Text('Bloquear'),
      value: _bloquearcheackbox, 
      onChanged:(value){
       setState(() {
          _bloquearcheackbox =value;
       });
      }
    );
  }

  Widget _crearSwitch() {
    return SwitchListTile(
      title: Text('Bloquear'),
      value: _bloquearcheackbox, 
      onChanged:(value){
       setState(() {
          _bloquearcheackbox =value;
       });
      }
    );
  }
}