import 'package:flutter/material.dart';
class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  String _nombre = '';
  String _email  = '';
  String _pasword= '';
  String _fecha = '';
  String _opcionseleccionada = 'Volar';

  List<String> _poderes = ['Volar','rayosx','aliento'];
  TextEditingController _inputFieldDateController = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Input Page'),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(
          horizontal:10.0,
          vertical:20.0,
        ),
        children: <Widget>[
          _crearInput(),
          Divider(),
          _crearEmail(),
          Divider(),
          _crearPassword(),
          Divider(),
          _crearFecha(context),
          Divider(),
          _crearDropdown(),
          Divider(),            
          _crearPersona(),
          Divider(),
          
        ],
      ),
    );
  }

  Widget _crearInput() {

    return TextField(
     //autofocus: true,
     textCapitalization: TextCapitalization.sentences,
     decoration: InputDecoration(
       border: OutlineInputBorder(
         borderRadius: BorderRadius.circular(20.0),
       ),
       counter: Text('Letras  ${ _nombre.length }'),
       hintText:'Name of the person',
       helperText:'Write your name',
       suffixIcon: Icon(Icons.accessibility_new),
       icon: Icon(Icons.account_circle),
       labelText: 'Name',
     ),
       onChanged:(valor){
         setState(() {
           _nombre = valor;
         });
       }
    );

  }

  Widget _crearPersona() {
    return ListTile(
      title: Text('Your Name is: $_nombre'),
      subtitle: Text('Email & Password: $_email & $_pasword'),
      trailing: Text('$_opcionseleccionada'),
    );
  }

  Widget _crearEmail() {
     return TextField(
     keyboardType: TextInputType.emailAddress,
     decoration: InputDecoration(
       border: OutlineInputBorder(
         borderRadius: BorderRadius.circular(20.0),
       ),
       hintText:'Email',
       suffixIcon: Icon(Icons.alternate_email),
       icon: Icon(Icons.email),
       labelText: 'Email',
     ),
       onChanged:(valor){
         setState(() {
           _email = valor;
         });
       }
    );
  }

 Widget  _crearPassword() {
   return TextField(
     obscureText: true,
     decoration: InputDecoration(
       border: OutlineInputBorder(
         borderRadius: BorderRadius.circular(20.0),
       ),
       hintText:'Password',
       suffixIcon: Icon(Icons.lock_outline),
       icon: Icon(Icons.lock),
       labelText: 'Password',
     ),
       onChanged:(valor){
         setState(() {
           _pasword = valor;
         });
       }
    );
 }

  _crearFecha(BuildContext context) {
    return TextField(
      controller: _inputFieldDateController,
     enableInteractiveSelection: false,
     decoration: InputDecoration(
       border: OutlineInputBorder(
         borderRadius: BorderRadius.circular(20.0),
       ),
       hintText:'Fecha de nacimiento',
       suffixIcon: Icon(Icons.calendar_today),
       icon: Icon(Icons.perm_contact_calendar),
       labelText: 'Fecha de nacimiento',
     ),
       onTap: () {
         FocusScope.of(context).requestFocus(new FocusNode());
         _selectDate(context);
       },
    );
  }
  List<DropdownMenuItem<String>> getOpcionesDropDown(){
    List<DropdownMenuItem<String>> lista = new List();
    _poderes.forEach((poder) { 
      lista.add(DropdownMenuItem(
        child: Text(poder),
        value: poder,
        )
      );
    });
    return lista;
  }
  Widget _crearDropdown(){
      return Row(
        children: <Widget>[
          Icon(Icons.select_all),
          SizedBox(
            width: 30.0,
          ),
          Expanded(
            child: DropdownButton(
              value: _opcionseleccionada,
              items: getOpcionesDropDown(), 
              onChanged:(opt){
                setState(() {
                  _opcionseleccionada = opt;
            });
        },
        
      ),
          )
    ],
   );
    
    
  }
 _selectDate(BuildContext context) async {
    DateTime picked = await showDatePicker(
      context: context, 
      initialDate:  new DateTime.now(), 
      firstDate:    new DateTime(2018), 
      lastDate:     new DateTime(2025),
      locale: Locale('es','ES'),
    );
      if(picked != null){
        setState(() {
          _fecha = picked.toString();
          _inputFieldDateController.text = _fecha;
        });
     }

  }
}