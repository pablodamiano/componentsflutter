import 'package:flutter/material.dart';

import 'package:components_futer/src/routes/routes.dart';
import 'package:components_futer/src/pages/card_page.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        localizationsDelegates: [
          // ... app-specific localization delegate[s] here
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en','US'), // English
          const Locale('es','ES'), // Hebrew
         
        ],
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      //home:HomePage(),
      initialRoute:'/',
      routes:getAplicationRoutes(),
      onGenerateRoute: (RouteSettings settings){
          print('Ruta Llamada: ${settings.name}');
          return MaterialPageRoute(
            builder:(BuildContext context)=>CardPage(), 
            );
      },
     
    );
  }
} 